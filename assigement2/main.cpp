#include <iostream>
#include<conio.h>
#include<stdio.h>
#include<iomanip>
using namespace std;
class LOPHOC;
class NGUOI
{
protected:
    char ht[50];
    char nsinh[20];
    char que[50];
public:
    void nhap();
    void xuat();
};
void NGUOI::nhap()
{
    cout<<"ho va ten: ";    fflush(stdin);  gets(ht);
    cout<<"ngay sinh: ";    fflush(stdin);  gets(nsinh);
    cout<<"que quan: ";     fflush(stdin);  gets(que);
}
void NGUOI::xuat()
{
    cout<<setw(10)<<ht<<setw(10)<<nsinh<<setw(10)<<que;
}
class SINHVIEN: public NGUOI
{
    char msv[20];
    char nganh[20];
    int kh;
public:
    void nhap();
    void xuat();
   // friend class LOPHOC;
    friend int dem(LOPHOC b);
    friend void sxep(LOPHOC b);
};
void SINHVIEN::nhap()
{
    NGUOI::nhap();
    cout<<"MSV: ";  fflush(stdin);  gets(msv);
    cout<<"nganh hoc: ";    fflush(stdin);  gets(nganh);
    cout<<"khoa hoc: "; cin>>kh;
}
void SINHVIEN::xuat()
{
    cout<<setw(10)<<msv;
    NGUOI::xuat();
    cout<<setw(10)<<nganh<<setw(10)<<kh<<endl;
}
class LOPHOC
{
    char malh[20];
    char tenlh[50];
    char date[20];
    SINHVIEN *x;
    int n;
    char gv[50];
public:
    void nhap();
    void xuat();
    friend int dem(LOPHOC b);
    friend void sxep(LOPHOC b);
};
void LOPHOC::nhap()
{
    cout<<" ma lop hoc: "; fflush(stdin);   gets(malh);
    cout<<"ten lop hoc: ";  fflush(stdin);  gets(tenlh);
    cout<<"ngay mo: ";  fflush(stdin);  gets(date);
    cout<<" so sinh vien: "; cin>>n;
    x = new SINHVIEN[n];
    for(int i=0; i<n; i++)
      x[i].nhap();
    cout<<"giao vien: "; fflush(stdin); gets(gv);
}
void LOPHOC::xuat()
{
    cout<<"ma lop hoc: "<<malh<<endl;
    cout<<"ten lop hoc: "<<tenlh<<endl;
    cout<<"ngay mo: "<<date<<endl;
    cout<<setw(10)<<"MSV"<<setw(10)<<"ho ten"<<setw(10)<<"ngay sinh"<<setw(10)<<"que quan";
    cout<<setw(10)<<"nganh"<<setw(10)<<"khoa hoc"<<endl;
    for(int i=0; i<n; i++)
       x[i].xuat();
    cout<<"giao vien: "<<gv<<endl;
}
int dem(LOPHOC b)
{
    int d=0;
    for(int i=0;i<b.n;i++)
        if(b.x[i].kh == 11)
            d++;
    return d;
}
void sxep(LOPHOC b)
{
    for(int i=0; i<b.n; i++)
        for(int j=b.n-1; j>i; j--)
            if(b.x[j-1].kh>b.x[j].kh)
            {
                SINHVIEN tg= b.x[j-1]; b.x[j-1]= b.x[j]; b.x[j]=tg;
            }
}
int main()
{
    LOPHOC a;
    a.nhap();
    a.xuat();
    cout<<endl;
    cout<<"so sv khoa 11: "<<dem(a)<<endl;
    cout<<endl;
    sxep(a);
    a.xuat();
    return 0;
}
