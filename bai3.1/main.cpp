#include <iostream>
#include<stdio.h>
#include<iomanip>
#include<conio.h>
using namespace std;
class DATE
{
    int D,M,Y;
public:
    void nhap();
    void xuat();
};
void DATE::nhap()
{
    cout<<"Day: ";  cin>>D;
    cout<<"Month: ";    cin>>M;
    cout<<"Year: ";     cin>>Y;
}
void DATE::xuat()
{
    cout<<D<<"/"<<M<<"/"<<Y<<endl;
}
class NHANSU
{
    char mans[20];
    char ht[50];
    DATE NS;
public:
    void nhap();
    void xuat();
};
void NHANSU::nhap()
{
    cout<<"ma nhan su: ";   fflush(stdin);  gets(mans);
    cout<<"ho ten: ";       fflush(stdin);  gets(ht);
    NS.nhap();
}
void NHANSU::xuat()
{
    cout<<"Ma nha su: "<<mans<<endl;
    cout<<"ho va ten: "<<ht<<endl;
    cout<<"ngay sinh: ";
    NS.xuat();
}
int main()
{
    NHANSU x;
    x.nhap();
    x.xuat();
    return 0;
}
