#include <iostream>
#include<iomanip>
#include<stdio.h>
using namespace std;
class PHIEU;
class NCC
{
    char mancc[20];
    char tenncc[40];
    char dc[50];
    char sdt[15];
public:
    void nhap();
    void xuat();
};
void NCC::nhap()
{
    cout<<"ma nha cung cap: ";  fflush(stdin);  gets(mancc);
    cout<<"ten nha cung cap: "; fflush(stdin);  gets(tenncc);
    cout<<"dia chi: ";          fflush(stdin);  gets(dc);
    cout<<"so dien thoai: ";    fflush(stdin);  gets(sdt);
}
void NCC::xuat()
{
    cout<<"Ma nha cung cap: "<<mancc<<setw(30)<<"Ten nha cung cap: "<<tenncc<<endl;
    cout<<"Dia chi:   "<<dc<<setw(30)<<"So dien thoai: "<<sdt<<endl;
}
class SP
{
    char masp[20];
    char tensp[50];
    int solg;
    float gia;
public:
    void nhap();
    void xuat();
    friend class PHIEU;
    friend void DEM(PHIEU a);
    friend void SAP(PHIEU a);
};
void SP::nhap()
{
    cout<<"ma san pham: ";  fflush(stdin);  gets(masp);
    cout<<"ten san pham: "; fflush(stdin);  gets(tensp);
    cout<<"so luong: ";     cin>>solg;
    cout<<"don gia: ";      cin>>gia;
}
void SP::xuat()
{
    cout<<setw(15)<<masp<<setw(15)<<tensp<<setw(10)<<solg<<setw(15)<<gia<<setw(15)<<gia*solg<<endl;
}
class PHIEU
{
    char ma[20];
    char ngay[20];
    NCC X;
    SP *Y;
    int n;
    float tong;
public:
    void nhap();
    void xuat();
    friend void DEM(PHIEU a);
   friend void SAP(PHIEU a);
};
void PHIEU::nhap()
{
    cout<<"ma phieu: "; fflush(stdin);  gets(ma);
    cout<<"ngay: ";     fflush(stdin);  gets(ngay);
    X.nhap();
    cout<<"so san pham: ";  cin>>n;
    Y= new SP[n];
    for(int i=0;i<n;i++)
        Y[i].nhap();
        tong=0;
        for(int i=0;i<n;i++)
            tong+= (Y[i].gia)*(Y[i].solg);

}
void PHIEU::xuat()
{
    cout<<"ma phieu: "<<ma<<setw(20)<<"Ngay: "<<ngay<<endl;
    X.xuat();
    cout<<setw(15)<<"Ma san pham"<<setw(15)<<"Ten san pham"<<setw(10)<<"So luong"<<setw(15)<<"Don gia"<<setw(15)<<"Thanh tien"<<endl;
    for(int i=0;i<n;i++)
        Y[i].xuat();
    cout<<setw(20)<<"TONG:"<<setw(15)<<tong<<endl;
}
void DEM(PHIEU a)
{
    int d=0;
    for(int i=0;i<a.n;i++)
        if(a.Y[i].solg<80)
            d++;
    if(d==0) cout<<"khong co sp nao co so luong duoi 80"<<endl;
    else    cout<<"so sp co so luong nhap nho hon 80 la: "<<d<<endl;
}
void SAP(PHIEU a)
{
    for(int i=0;i<a.n;i++)
        for(int j=a.n-1;j>i;j--)
            if(a.Y[j-1].gia > a.Y[j].gia)
            {
                SP Z= a.Y[j-1]; a.Y[j-1] = a.Y[j]; a.Y[j]=Z;
            }
}
int main()
{
    PHIEU a;
    a.nhap();
    a.xuat();
    DEM(a);

    cout<<endl;
    SAP(a);
    a.xuat();
    return 0;
}
